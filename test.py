#!/usr/bin/env python3

import os
import pytest
from queue import Queue

from librelay import LibRelay02, RelayQueries, RelayStates, APIAction

args = {
    "device": "/dev/ttyACM0",
    "bauds": 115200,
}


def test_unthreaded_apis():
    lr = LibRelay02(args["device"], int(args["bauds"]))
    if lr.setup():
        print(f"[I]: Version {lr.version()}")
        print(f"[I]: Version {lr.status()}")
        lr.all_on()
        lr.all_off()
        lr.one_on()
        lr.one_off()
        lr.two_on()
        lr.two_off()
        return True
    else:
        return False

def test_generate_documentation():
    os.system("python3 -m pdoc librelay -o doc")

def test_threaded_apis():
    lr = LibRelay02(args["device"], int(args["bauds"]))
    q, r = Queue(), Queue()
    if lr.setup():
        lr.set_input_queue(q)
        lr.set_output_queue(r)
        lr.start()
        q.put({"action": APIAction.QUERY.name, "query": RelayQueries.VERSION.name})
        print(r.get())
        q.put({"action": APIAction.SET_STATE.name, "query": RelayStates.ALL_ON.name})
        print(r.get())
        q.put({"action": APIAction.SET_STATE.name, "query": RelayStates.ALL_OFF.name})
        print(r.get())
        q.put({"action": APIAction.SET_STATE.name, "query": RelayStates.ONE_ON.name})
        print(r.get())
        q.put({"action": APIAction.SET_STATE.name, "query": RelayStates.ONE_OFF.name})
        print(r.get())
        q.put({"action": APIAction.SET_STATE.name, "query": RelayStates.TWO_ON.name})
        print(r.get())
        q.put({"action": APIAction.SET_STATE.name, "query": RelayStates.TWO_OFF.name})
        print(r.get())
        lr.quit()
        return True
    else:
        return False

